#!/usr/bin/env python


import math
from gimpfu import *

def python_overlay_adjustment(im, tdrawable, color=gimpcolor.RGB(255,255,255)):
	im.undo_group_start()

	layer_one = gimp.Layer(im, "Overlay", im.width, im.height, 
	                       RGBA_IMAGE, 100, OVERLAY_MODE)
	im.add_layer(layer_one, 0)

	bg = pdb.gimp_context_get_background()
	pdb.gimp_context_set_background(color)
	pdb.gimp_edit_fill(layer_one, BACKGROUND_FILL)
	pdb.gimp_context_set_background(bg)

	ma = layer_one.create_mask(ADD_BLACK_MASK)
	layer_one.add_mask(ma)

	im.undo_group_end()


register(
		"python_fu_overlay_adjustment",
		"Adds a white layer with transparent mask in the top with overlay blending",
		"Adds a white layer with transparent mask in the top with overlay blending",
		"Mikel Garai",
		"Mikel Garai",
		"2011",
		"<Image>/Filters/_Overlay adjustment...",
		"RGB*, GRAY*",
		[
				(PF_COLOR, "color", "color", gimpcolor.RGB(255,255,255)),
		],
		[],
		python_overlay_adjustment)

main()
