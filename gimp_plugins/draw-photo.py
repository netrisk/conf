#!/usr/bin/env python


import math
from gimpfu import *

def python_draw_photo(im, tdrawable, sel_gaus = 5.0, num_mul = 4):
	im.undo_group_start()

	l1 = tdrawable.copy()
	l1.mode = NORMAL_MODE

	im.add_layer(l1, 0)
	pdb.gimp_hue_saturation(l1, ALL_HUES, 0,0,-100)
	
	l2 = l1.copy()
	im.add_layer(l2, 0)

	pdb.plug_in_sel_gauss(im, l2, sel_gaus, 50.0)

	pdb.gimp_invert(l2)
	l1.opacity = 100.0
	l2.opacity = 50.0

	l1 = im.merge_down(l2, EXPAND_AS_NECESSARY)

	l2 = l1.copy()
	im.add_layer(l2, 0)

	l2.mode = DODGE_MODE

	l1 = im.merge_down(l2, EXPAND_AS_NECESSARY)

	i = 0
	while (i < num_mul):
		l2 = l1.copy()
		im.add_layer(l2, 0)

		l2.mode = MULTIPLY_MODE
		l1 = im.merge_down(l2, EXPAND_AS_NECESSARY)
		i += 1


	im.undo_group_end()


register(
		"python_fu_draw_photo",
		"Makes another layer on top of current one that seems to be drawn",
		"Makes another layer on top of current one that seems to be drawn",
		"Mikel Garai",
		"Mikel Garai",
		"2011",
		"<Image>/Filters/_Draw Photo",
		"RGB*, GRAY*",
		[
				(PF_FLOAT,   "sel_gaus", "selective blur radius", 5.0),
				(PF_INT,   "num_mul", "num multiplies", 4),
		],
		[],
		python_draw_photo)

main()
