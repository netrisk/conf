#!/usr/bin/env python


import math
from gimpfu import *

def python_copy_with_dark_mask(im, tdrawable, blur_radius=40):
	im.undo_group_start()

	layer_2 = tdrawable.copy()
	im.add_layer(layer_2, 0)
	ma = layer_2.create_mask(ADD_COPY_MASK)
	layer_2.add_mask(ma)

	pdb.plug_in_gauss(im, ma, blur_radius, blur_radius, 1)

	pdb.gimp_invert(ma)


	im.undo_group_end()


register(
		"python_fu_copy_with_dark_mask",
		"Add a layer on top as a copy of the current one with a mask that is the inverted blurred copy of it",
		"Add a layer on top as a copy of the current one with a mask that is the inverted blurred copy of it",
		"Mikel Garai",
		"Mikel Garai",
		"2011",
		"<Image>/Filters/_Copy with dark mask...",
		"RGB*, GRAY*",
		[
				(PF_INT,   "blur_radius", "blur radius (pixels, 0 == disabled)", 40),
		],
		[],
		python_copy_with_dark_mask)

main()
