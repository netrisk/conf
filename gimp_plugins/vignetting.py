#!/usr/bin/env python


import math
from gimpfu import *

def python_vignetting(im, tdrawable, border_width=250, 
                      blur=1000, color=gimpcolor.RGB(0,0,0),
                      select=False):
	im.undo_group_start()
	if (border_width *2 > im.width):
		border_width = im.width / 2 - 1
	if ( border_width *2 > im.height):
		border_width = im.height / 2 - 1


	pdb.gimp_rect_select(im, border_width, border_width, 
	                     im.width - border_width*2, 
	                     im.height - border_width*2, 
	                     CHANNEL_OP_REPLACE, False, 0)

	pdb.gimp_selection_feather(im, blur)


	pdb.gimp_selection_invert(im)


	if (not select):
		layer_one = gimp.Layer(im, "Vignetting", im.width, im.height, 
		                       RGBA_IMAGE, 50, OVERLAY_MODE)
		im.add_layer(layer_one, 0)
		bg = pdb.gimp_context_get_background()
		pdb.gimp_context_set_background(color)
		pdb.gimp_edit_fill(layer_one, BACKGROUND_FILL)
		pdb.gimp_context_set_background(bg)

		pdb.gimp_selection_clear(im)

	im.undo_group_end()


register(
		"python_fu_vignetting",
		"Add a layer on top with a vignetting effect",
		"Add a layer on top with a vignetting effect or make a selection with gausian borders",
		"Mikel Garai",
		"Mikel Garai",
		"2011",
		"<Image>/Filters/_Vignetting...",
		"RGB*, GRAY*",
		[
				(PF_INT,   "border_width", "border width (pixels)", 250),
				(PF_INT,   "blur", "blur (pixels)", 1000),
				(PF_COLOR, "color", "color", gimpcolor.RGB(0,0,0)),
				(PF_BOOL,  "select", "Just make selection", False),
		],
		[],
		python_vignetting)

main()
